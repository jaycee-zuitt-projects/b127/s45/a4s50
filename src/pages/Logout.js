import { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';



export default function Logout(){

	const { setUser, unsetUser } = useContext(UserContext)

	//Clear the localStorage of the user's information
	unsetUser();

	//placing the 'setUser' setter function inside of a useEffect is necessary becuase of updates within react js that a state of another component cannot be updated while trying to render a different component.
	//By adding the useEffect, this will allow the Logout page to render first before triggering the useEffect which changes the state of our user.
	useEffect(() => {
		//Set the user state back to it's original value
		setUser({ accessToken: null })
	}, [])
	//the array in our useEffect provide initial rendering only.

	return(
		//Redirect back to login
		<Redirect to="/login"/>
		)
}