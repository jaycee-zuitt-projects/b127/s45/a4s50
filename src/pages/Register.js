import { Fragment, useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Register(){
	const history = useHistory();

	const { user, setUser } = useContext(UserContext); 
	//forms
	const [ firstName, setFirstName ] = useState("");
	const [ lastName, setLastName ] = useState("");
	const [ email, setEmail ] = useState("");
	const [ mobileNo, setMobileNo ] = useState("");
	const [ password, setPassword ] = useState("");
	const [ password2, setPassword2 ] = useState("");
	//button
	const [ isActive, setIsActive ] = useState(false);

	useEffect(() => {
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && password2 !== "") && (mobileNo.length >= 11) && (password === password2)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [firstName, lastName, email, mobileNo, password, password2])

	function registerUser(e){
		e.preventDefault();
		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data === true){
				Swal.fire({
					title: 'Oh no!',
					icon: 'error',
					text: 'Email already exists',
				})
			}else{
				fetch("http://localhost:4000/users/register", {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					mobileNo: mobileNo,
					password: password 
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)
				if(data === true){
					Swal.fire({
						title: "Hoooray!!!!",
						icon: "success",
						text: "You have successfully registered"
					})
					history.push("/login")
				}else{
					Swal.fire({
						title: "Opppssss!!!",
						icon: "error",
						text: "Something went wrong. Check your Credentials"
					})
				}
				
			})
				setFirstName("")
				setLastName("")
				setEmail("")
				setMobileNo("")
				setPassword("")
				setPassword2("")

			}
		})
			

	}

	return(
		(user.accessToken !== null)?
		<Redirect to="/" />
		:
		<Fragment>
		<h1>Register</h1>
		<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group>
				<Form.Label>First Name:</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter First name"
					value={firstName}
					onChange={e => setFirstName(e.target.value)} 
					required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Last Name:</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter Last Name"
					value={lastName}
					onChange={e => setLastName(e.target.value)} 
					required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Email address:</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter Email"
					value={email}
					onChange={e => setEmail(e.target.value)} 
					required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Mobile Number:</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter Mobile Number"
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)} 
					required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your Password"
					onChange={e => setPassword(e.target.value)}
					value={password}
					required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Verify Password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify your Password"
					onChange={e => setPassword2(e.target.value)}
					value={password2}
					required
				/>
			</Form.Group>
			{isActive ?
			<Button variant="primary" type="submit" id="submitBtn">
				Submit
			</Button>
			:
			<Button variant="primary" type="submit" id="submitBtn" disabled>
				Submit
			</Button>
			}

		</Form>
	</Fragment>
	)

}
/*================================================================================*/
/* old code
import { Fragment, useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Register(){
	//State hooks to store the values of the input fields

	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const { user } = useContext(UserContext)

	const [isActive, setIsActive] = useState(false);

	//check if values are successfully binded
	console.log(email);
	console.log(password1);
	console.log(password2);

	useEffect(()=>{
		//Validation to enable submit button when all fields are populated and both passwords match
		if((email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [email, password1, password2])

	function registerUser(e){
		e.preventDefault();
		//to clear out the data in our input fields
		setEmail("")
		setPassword1("")
		setPassword2("")

		Swal.fire({
			title: "Hoooray!!!!",
			icon: "success",
			text: "You have successfully registered"
		})
	}

	//Two way binding
	//The values in the fields of the force is bound to the getter of the state and the event is bound to the setter. this is called two way binding
	//The data we changed in the view has updated the state
	//The data in the state has updated the view



	return(
	(user.accessToken !== null)?
			<Redirect to="/" />
			:
	<Fragment>
		<h1>Register</h1>
		<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group>
				<Form.Label>Email address:</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter Email"
					value={email}
					onChange={e => setEmail(e.target.value)} //the e.target.value property allows us to gain access to the input fields current value to be used when submitting form data.
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your Password"
					onChange={e => setPassword1(e.target.value)}
					value={password1}
					required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Verify Password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify your Password"
					onChange={e => setPassword2(e.target.value)}
					value={password2}
					required
				/>
			</Form.Group>
			{isActive ?
			<Button variant="primary" type="submit" id="submitBtn">
				Submit
			</Button>
			:
			<Button variant="primary" type="submit" id="submitBtn" disabled>
				Submit
			</Button>
			}

		</Form>
	</Fragment>
	)
} */