import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Link, useHistory, useParams } from 'react-router-dom';

import UserContext from '../UserContext';


const SpecificCourse = () => {

    const { user } = useContext(UserContext);

    const [ name, setName ] = useState('')
    const [ description, setDescription ] = useState('')
    const [ price, setPrice ] = useState(0)

    const { courseId } = useParams();//useParams() contains any values that we are trying to pass in the URL stored in a key/value pair
    //useParams is how we receive the courseId passed via the URL

    useEffect(() => {
        fetch(`http://localhost:4000/courses/${ courseId }`)
        .then(res => res.json ())
        .then(data => {
            setName(data.name)
            setDescription(data.description)
            setPrice(data.price)
        })
    }, [])

    const enroll = () => {
        fetch('http://localhost:4000/users/enroll', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
            },
            body: JSON.stringify({
                courseId: courseId
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Swal.fire({
                    title:"Way To Go!!!!",
                    icon: "success",
                    text: `You have successfully enrolled for this ${ name } course.`
                })
            }else{
                Swal.fire({
                    title:"Opsss!!!!",
                    icon: "error",
                    text: "Something Went Wrong, please try again"
                })
            }
        })
    }
    
    return (
        <Container>
            <Card>
                <Card.Header className='bg-dark text-white text-center pb-0'>
                    <h4>{name}</h4>
                </Card.Header>

                <Card.Body className="text-center">
                    <Card.Text>
                        {description}
                    </Card.Text>
                    <h6>Php {price}</h6>
                </Card.Body>

                <Card.Footer>

                    {
                        user.accessToken !== null ?
                             <Button vaiant="primary" block onClick={() => enroll(courseId)}>Enroll</Button>
                            :
                            <Link className="btn btn-warning btn-block" to="/login">Login to Enroll</Link>

                    }
                    
                </Card.Footer>
            </Card>
        </Container>
    )
}

export default SpecificCourse
