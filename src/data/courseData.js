const coursesData = [

	{
		id: "wdc001",
		name: "PHP - Laravel",
		desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commod consequat. Duis aute irure dolor in reprehenderit in voluptate velit essecillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupid0atat nonproident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
		price: 45000,
		onOffer: true
	},

	{
		id: "wdc002",
		name: "Python - Django",
		desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commod consequat. Duis aute irure dolor in reprehenderit in voluptate velit essecillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupid0atat nonproident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
		price: 50000,
		onOffer: true
	},

	{
		id: "wdc003",
		name: "Java - Springboot",
		desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commod consequat. Duis aute irure dolor in reprehenderit in voluptate velit essecillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupid0atat nonproident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
		price: 55000,
		onOffer: true
	},

	{
		id: "wdc004",
		name: "Javascript",
		desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commod consequat. Duis aute irure dolor in reprehenderit in voluptate velit essecillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupid0atat nonproident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
		price: 40000,
		onOffer: true
	}


]

export default coursesData;