import { useState, useEffect, Fragment } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView(props){
	const { coursesData, fetchData } = props

	const [ courses, setCourses ] = useState([])
	const [ name, setName ] = useState("");
	const [ description, setDescription ] = useState("");
	const [ price, setPrice ] = useState("");
	//state for addCourse modal
	const [show, setShow] = useState(false);
	//state for update modal
	const [showEdit, setShowEdit] = useState(false);

	//Add a state for courseId for the fetch URL
	const [courseId, setCourseId] = useState('')

	//modal for addcourse
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	//modal for edit = Function to handle closing in our edit course modal. We need to reset all relevant states back to their default values.
	const closeEdit = () => {
		setShowEdit(false)
		setName('')
		setDescription('')
		setPrice(0)
	}
	const openEdit = (courseId) => {
		fetch(`http://localhost:4000/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			//Populate all input values with the course information that we fetched
			setCourseId(data._id)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

		//then open the modal
		setShowEdit(true)
	}

	

	useEffect(() => {
		const coursesArr = coursesData.map(course => {
			return(
				<tr key={course._id}>
					<td>{course._id}</td>
					<td>{course.name}</td>
					<td>{course.description}</td>
					<td>{course.price}</td>
					<td className={course.isActive ? "text-success" : "text-danger"}>
					{course.isActive ? "Available" : "Unavailable"}
					</td>
					<td>
						<Button variant="primary" size="sm" onClick={() => openEdit(course._id)}>Update</Button>

						{course.isActive ?
						<Button variant="danger" size="sm" onClick={() => archiveToggle(course._id, course.isActive)}>Disable</Button>
						:
						<Button variant="success" size="sm" onClick={() => activateToggle(course._id, course.isActive)}>Enable</Button>
						}
						
					</td>
				</tr>
				)
		})
		setCourses(coursesArr)
	}, [coursesData])


	// add new course function

	const addCourse = (e) => {
		e.preventDefault();
		fetch('http://localhost:4000/courses/', {
			method: "POST",
			headers:{
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true){
				//Run our fetchData function that we passed our parante component, in order to re-render our page
				fetchData()

				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Course successfully added.'
				})
				setName('')
				setDescription('')
				setPrice(0)

				handleClose()

			}else{
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'error',
					text: 'Please Try Again'
				})
			}
		})
	}

	//edit course
	const editCourse = (e, courseId) => {
		e.preventDefault();
		fetch(`http://localhost:4000/courses/${courseId}`, {
			method: "PUT",
			headers:{
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				fetchData()
				Swal.fire({
					title: "SUccess!",
					icon: "success",
					text: "Course successfully updated"
				})
				closeEdit()
			}else{
				fetchData()
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}

	// Archive or disable a course
	const archiveToggle = (courseId, isActive) => {
		fetch(`http://localhost:4000/courses/${courseId}/archive`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				fetchData()
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Course successfully disabled'
				})
			}else{
				fetchData()
				Swal.fire({
					title: 'Somethin went wrong',
					icon: 'error',
					text: 'Please Try Again'
				})
			}
		})
	}

	//Activate course
	const activateToggle = (courseId, isActive) => {
        fetch(`http://localhost:4000/courses/${courseId}/activate`,{
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem("accessToken")}`
            },
            body: JSON.stringify({
                isActive: isActive
            })
        })
        .then(res => res.json())
        .then(data => {
			console.log(data)
            if(data === true){
                fetchData()
                Swal.fire({
                    title:"Successfully activated!!!",
                    icon: "success",
                    text:"course successfully activated"
                })
            }else{
                fetchData()
                Swal.fire({
                    title:"Error!!!",
                    icon: "error",
                    text: "activate error!!!"
                })
            }
        })
    }

	return(
	<Fragment>
		<div className="text-center my-4"> 
			<h2>Admin Dashboard</h2>
			<div className="d-flex justify-content-center">
				<Button variant="primary" onClick={handleShow}>Add New Course</Button>
			</div>
		</div>
		<Table striped bordered hover responsive>
			<thead className="bg-dark text-white">
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Description</th>
					<th>Price</th>
					<th>Availability</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				{courses}
			</tbody>
		</Table>
		{/* add modal for new course */}
		<Modal show={show} onHide={handleClose}>
				<Form onSubmit={e => addCourse(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Course</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name:</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description:</Form.Label>
							<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price:</Form.Label>
							<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={handleClose}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
		</Modal>

		{/* Edit course modal */}
		<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editCourse(e, courseId)}>
					<Modal.Header closeButton>
						<Modal.Title>Update Course</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name:</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description:</Form.Label>
							<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price:</Form.Label>
							<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
		</Modal>
	</Fragment>


		)
}